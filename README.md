#TravelFare API 
=================
 This project developed with below tech stack
- Java 1.8 
- Angular Version **12.2.16**
- Node Version **14.15.5**
	
#Summary 
 This Travelfare API provides the list of airport information.FareDetails are calculated based on origin and destination.


# Demo 
 Clone this repo and start it (on windows systems use the gradlew.bat file):

 `./gradlew bootRun` or './gradlew build' & java -jar /build/libs/destination-finder-client-0.1.0.jar

to list all tasks:

 `./gradlew tasks`

 To view the assignment (after starting the application) go to:

 [http://localhost:8090](http://localhost:8090)

 To retrieve the available airports

 **Retrieve a list of airports**:

`http://localhost:8090/airport/all`

Query params:

- size: the size of the result
- page: the page to be selected in the paged response
- lang: the language, supported ones are nl and en
- term: A search term that searches through code, name and description.

**Retrieve a fare details**:

`http://localhost:8090/fares/{origin_code}/{destination_code}`

Query params:

- currency: the requested resulting currency, supported ones are EUR and USD

## Given more time,Would implement the following 

- Endpoint Metrics
- Swagger API
- FrontEnd Designs

## Author
[Prbau Thangavel]

