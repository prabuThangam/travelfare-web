package com.afkl.cases.df;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

import java.io.IOException;
/**
 * TravelFare application
 *
 * @author Prabu Thangavel
 *
 */
@EnableEncryptableProperties
@SpringBootApplication
public class TravelFareApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(TravelFareApplication.class, args);
	}

}
