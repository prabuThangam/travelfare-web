package com.afkl.cases.df.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Getter
@Configuration
public class AppServiceConfig {

    @Value("${fareService}")
    private String fareServiceUrl;

    @Value("${airportService}")
    private String airportServiceUrl;

    @Value("${allAirportService}")
    private String allAirportUrl;
}
