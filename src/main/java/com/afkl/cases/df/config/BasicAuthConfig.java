package com.afkl.cases.df.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.web.client.RestTemplate;

@Profile("!test")
@Configuration
public class BasicAuthConfig {

    @Value("${basicAuthConfig.clientId}")
    private String clientId;

    @Value("${basicAuthConfig.secret}")
    private String secret;


    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor(clientId, secret));
        return restTemplate;
    }


}
