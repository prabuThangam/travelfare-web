import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClientService } from '../core/services/http-client.service';
import { debounceTime, pluck, takeUntil } from 'rxjs/operators';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { orderBy } from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { StationDetailsComponent } from './station-details/station-details.component';


@Component({
  selector: 'app-airports',
  templateUrl: './airports.component.html',
  styleUrls: ['./airports.component.scss']
})
export class AirportsComponent implements OnInit, OnDestroy {
  public searchForm: FormGroup;
  public airportsDataSource: MatTableDataSource<
    Location[]
  >;
  public displayedColumns: string[] = ['code', 'name', 'description', 'view'];

  #destroy$ = new Subject();
  constructor(
    private formBuilder: FormBuilder,
    private httpClientService: HttpClientService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    // Init the form
    this.searchForm = this.formBuilder.group({
      searchTerm: [null]
    });

    // Listen to form changes
    this.searchForm.valueChanges.subscribe(value => {
      this.getAirports(value.searchTerm);
    });
  }

  ngOnDestroy(): void {
    this.#destroy$.next();
    this.#destroy$.unsubscribe();
  }

  public getAirports(searchTerm: string): void {
    this.httpClientService
      .getAirportList({
        lang: 'EN',
        term: searchTerm
      })
      .pipe(
        debounceTime(5000),
        pluck('_embedded', 'locations'),
        takeUntil(this.#destroy$)
      )
      .subscribe(airports => {
        const result = orderBy(airports, ['code'], ['asc']);

        this.airportsDataSource = new MatTableDataSource<
          Location[]
        >(result as any[]);
      });
  }

  public viewStationInformation(stationCode: string): void {
    this.httpClientService
      .getAirportDetails(stationCode, {})
      .pipe(takeUntil(this.#destroy$))
      .subscribe({
        next: stationDetails => {
          this.dialog.open(StationDetailsComponent, {
            data: stationDetails._embedded.locations.find(term=> term.code === stationCode)
          });
        },
        error: error => {
          console.error(error);
        }
      });
  }
}
