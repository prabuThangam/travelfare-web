import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { AirportsComponent } from './airports.component';
import { StationDetailsComponent } from './station-details/station-details.component';

@NgModule({
  declarations: [AirportsComponent, StationDetailsComponent],
  imports: [
    CommonModule,
    SharedModule,

    RouterModule.forChild([
      {
        path: '',
        component: AirportsComponent
      }
    ])
  ]
})
export class AirportsModule {}
