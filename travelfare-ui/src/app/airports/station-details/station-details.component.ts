import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import {Location} from "../../shared/models/airports.types";

@Component({
  selector: 'app-station-details',
  templateUrl: './station-details.component.html',
  styleUrls: ['./station-details.component.scss']
})
export class StationDetailsComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA)
    public stationDetails: Location
  ) {}
}
