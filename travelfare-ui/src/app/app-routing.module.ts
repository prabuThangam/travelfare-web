import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./airports/airports.module').then(a => a.AirportsModule)
  },
  {
    path: 'fares-details',
    loadChildren: () =>
      import('./fares-details/fares-details.module').then(
        f => f.FaresDetailsModule
      )
  },
  {
    path: '',
    redirectTo: '/airports',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/airports'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
