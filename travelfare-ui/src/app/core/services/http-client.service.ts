import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Airports, SearchParams } from 'src/app/shared/models/airports.types';
import { Observable } from 'rxjs';
import { Fares } from 'src/app/shared/models/fares.types';
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(private httpClient: HttpClient) {}

  public getAirportList(params?: SearchParams): Observable<Airports> {
    const url = params.term ?`/airport/all?term=${params?.term}&size=100`:'/airport/all?size=100';
    return this.httpClient.get<Airports>(
      `${environment.baseUrl}${url}`
    );
  }

  public getAirportDetails(
    station: string,
    params: SearchParams
  ): Observable<Airports> {
    return this.httpClient.get<Airports>(
      `${environment.baseUrl}/airport/all?term=${station}`
    );
  }

  public getFaresDetails(
    origin: string,
    destination: string,
    params: SearchParams
  ): Observable<Fares> {
    return this.httpClient.get<Fares>(
      `${environment.baseUrl}/fares/${origin}/${destination}?currency=${params.currency}`
    );
  }
}
