import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaresDetailsComponent } from './fares-details.component';

describe('FaresDetailsComponent', () => {
  let component: FaresDetailsComponent;
  let fixture: ComponentFixture<FaresDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FaresDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FaresDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
