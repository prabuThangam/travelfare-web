import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { pluck, takeUntil } from 'rxjs/operators';
import { HttpClientService } from '../core/services/http-client.service';
import { Location } from '../shared/models/airports.types';
import { Fares } from '../shared/models/fares.types';

@Component({
  selector: 'app-fares-details',
  templateUrl: './fares-details.component.html',
  styleUrls: ['./fares-details.component.scss']
})
export class FaresDetailsComponent implements OnInit, OnDestroy {
  public airports: string[] = [];
  public fareDetails: Fares;
  public searchForm: FormGroup;

  #destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private httpClientServer: HttpClientService
  ) {}

  ngOnInit(): void {
    this.searchForm = this.formBuilder.group({
      origin: [null, Validators.required],
      destination: [null, Validators.required],
      currency: [null, Validators.required]
    });

    this.httpClientServer
      .getAirportList({
        size: 100
      })
      .pipe(pluck('_embedded', 'locations'), takeUntil(this.#destroy$))
      .subscribe(response => this.initializeAirports(response));
  }

  ngOnDestroy(): void {
    this.#destroy$.next();
    this.#destroy$.unsubscribe();
  }

  public initializeAirports(
    airports: Location[]
  ): void {
    this.airports = airports.map(item => item.code).sort();
  }

  public getFareDetails(): void {
    this.httpClientServer
      .getFaresDetails(
        this.searchForm.value.origin,
        this.searchForm.value.destination,
        {
          currency: this.searchForm.value.currency
        }
      )
      .subscribe({
        next: response => (this.fareDetails = response),
        error: error => {
          console.error(error);
        }
      });
  }
}
