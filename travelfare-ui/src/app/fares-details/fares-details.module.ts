import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { FaresDetailsComponent } from './fares-details.component';

@NgModule({
  declarations: [FaresDetailsComponent],
  imports: [
    CommonModule,
    SharedModule,

    RouterModule.forChild([
      {
        path: '',
        component: FaresDetailsComponent
      }
    ])
  ]
})
export class FaresDetailsModule {}
