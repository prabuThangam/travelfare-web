export interface Airports {
  _embedded: Embedded;
  page: Page;
}

export interface Embedded {
  locations: Location[];
}

export interface Location {
  code: string;
  name: string;
  description: string;
  coordinates?: Coordinates;
  parent: Parent;
}

export interface Coordinates {
  latitude: number;
  longitude: number;
}

export interface Parent {
  code: string;
  name: string;
  description: string;
  coordinates: Coordinates;
  parent?: Parent;
}

export interface Page {
  size: number;
  totalElements: number;
  totalPages: number;
  number: number;
}

export interface SearchParams {
  size?: number;
  page?: number;
  lang?: 'EN' | 'NL';
  term?: string;
  currency?: 'EUR' | 'USD';
}
