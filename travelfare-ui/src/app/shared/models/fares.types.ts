export interface Fares {
  orgin:       Destination;
  destination: Destination;
  amount:      number;
  currency:    string;
}

export interface Destination {
  code:        string;
  name:        string;
  description: string;
  coordinates: Coordinates;
}

export interface Coordinates {
  latitude:  number;
  longitude: number;
}
