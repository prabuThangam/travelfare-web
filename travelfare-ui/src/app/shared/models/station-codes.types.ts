export interface StationCode {
  code: string;
  name: string;
  description: string;
  coordinates: Coordinates;
  parent: Parent;
}

export interface Coordinates {
  latitude: number;
  longitude: number;
}

export interface Parent {
  code: string;
  name: string;
  description: string;
  coordinates: Coordinates;
  parent?: Parent;
}
