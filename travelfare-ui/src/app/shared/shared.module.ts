import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ngMaterialModules } from './modules/material.module';

const sharedModules = [
  FormsModule,
  CommonModule,
  ReactiveFormsModule,
  ...ngMaterialModules
];

@NgModule({
  declarations: [],
  imports: [sharedModules],
  exports: [...sharedModules]
})
export class SharedModule {}
